﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2_N00038949.Models.DB.Configurations
{
    public class EntrenadoresConfig : IEntityTypeConfiguration<Entrenador>
    {

        public void Configure(EntityTypeBuilder<Entrenador> builder)
        {
            builder.ToTable("Entrenador");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Capturas).WithOne(o => o.Entrenador).HasForeignKey(o => o.EntrenadorId);
        }
    }
}
