﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2_N00038949.Models
{
    public class Captura
    {
        public int Id { get; set; }
        public int EntrenadorId { get; set; }
        public int PokemonId { get; set; }
        public DateTime Fecha { get; set; }
        public Entrenador Entrenador { get; set; }
        public Pokemon Pokemon { get; set; }
    }
}
