﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace T2_N00038949.Models
{
    public class Entrenador
    {
        public int Id { get; set; } 
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Ciudad { get; set; }
        [Required]
        public string Pass { get; set; }
        public List<Captura> Capturas { get; set; }
    }
}
