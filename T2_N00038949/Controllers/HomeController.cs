﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using T2_N00038949.Models;
using T2_N00038949.Models.DB;
using T2_N00038949.Models.Extensiones;

namespace T2_N00038949.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext context;
        public HomeController( AppDbContext _context) 
        {
            context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Login() 
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string nombre, string pass)
        {
            var entrenador = context.Entrenadores.FirstOrDefault(o => o.Nombre == nombre && o.Pass == pass);
            if (entrenador == null)
            {
                return View();
            }
            HttpContext.Session.Set("LoggedUser", entrenador);
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name,entrenador.Nombre),
            };
            var userIdentity = new ClaimsIdentity(claims, "login");
            var principal = new ClaimsPrincipal(userIdentity);
            HttpContext.SignInAsync(principal);
            return RedirectToAction("Index","Pokemon");
        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult Ver()
        {
            var entrenador = HttpContext.Session.Get<Entrenador>("LoggedUser");
            var model = context.Entrenadores.Include(o => o.Capturas).ThenInclude(o => o.Pokemon).FirstOrDefault(o => o.Id == entrenador.Id);

            ViewBag.EntrenadorId = entrenador.Id;
            return View(model);
        }
        [HttpGet]
        public IActionResult Registrarse()
        {
            ViewBag.Ciudad = Ciudad();
            return View(new Entrenador());
        }
        [HttpPost]
        public IActionResult Registrarse(Entrenador entrenador)
        {
            if (ModelState.IsValid)
            {
                context.Entrenadores.Add(entrenador);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ciudad = Ciudad();
            return View(entrenador);
        }
        private List<string> Ciudad()
        {
            return new List<string>
            {
                "Cajamarca","Chiclayo","Lima","Trujillo",
            };
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
