﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using T2_N00038949.Models;
using T2_N00038949.Models.DB;
using T2_N00038949.Models.Extensiones;

namespace T2_N00038949.Controllers
{
    [Authorize]
    public class PokemonController : Controller
    {
        private AppDbContext context;
        private IHostingEnvironment Env { get; }
        public PokemonController(IHostingEnvironment env, AppDbContext _context)
        {
            context = _context;
           Env = env;
        }
        [HttpGet]
        public IActionResult Index()
        {
           
            return View();
        }
        [HttpGet]
        public IActionResult _Index(string nombre)
        {
            var query = context.Pokemons.AsQueryable();
            if (!String.IsNullOrEmpty(nombre)) 
            {
                query = query.Where(o=>o.Nombre.Contains(nombre));
            }
            
            return View(query.ToList());
        }
        [HttpGet]
        public IActionResult Create() 
        {
            ViewBag.Tipo = Tipo();
            return View(new Pokemon());
        }
        [HttpPost]
        public IActionResult Create(Pokemon pokemon,IFormFile imagen)
        {
            if (context.Pokemons.Where(o => o.Nombre == pokemon.Nombre).Count() > 0)
                ModelState.AddModelError("Nombre", "Ya existe un pokemon con ese nombre");
            if (ModelState.IsValid) 
            {
                if (imagen.Length>0) 
                {
                    var filePath = Path.Combine(Env.WebRootPath, "images", imagen.FileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        imagen.CopyTo(stream);
                    }
                }
                pokemon.Imagen = imagen.FileName;
                context.Pokemons.Add(pokemon);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pokemon);
        }
        private List<string> Tipo()
        {
            return new List<string>
            {
                "Normal","Lucha","Volador","Veneno","Tierra","Roca","Bicho","Fantasma","Acero","Fuego","Agua","Planta","Electrico","Psiquico","Hielo","Dragon","Hada","Siniestro",
            };
        }
    }
}