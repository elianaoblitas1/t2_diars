﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2_N00038949.Models.DB.Configurations
{
    public class CapturaConfig : IEntityTypeConfiguration<Captura>
    {

        public void Configure(EntityTypeBuilder<Captura> builder)
        {
            builder.ToTable("Captura"); //Entre comillas el nombre de la tabla
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Pokemon).WithMany(o => o.Capturas).HasForeignKey(o => o.PokemonId);
        }
    }
}
