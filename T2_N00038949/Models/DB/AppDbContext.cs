﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2_N00038949.Models.DB.Configurations;

namespace T2_N00038949.Models.DB
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<Entrenador> Entrenadores { get; set; }
        public DbSet<Captura> Capturas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Configuracion de tablas
            modelBuilder.ApplyConfiguration(new PokemonConfig());
            modelBuilder.ApplyConfiguration(new EntrenadoresConfig());
            modelBuilder.ApplyConfiguration(new CapturaConfig());
        }
    }
}
