﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using T2_N00038949.Models;
using T2_N00038949.Models.DB;
using T2_N00038949.Models.Extensiones;

namespace T2_N00038949.Controllers
{
    public class CapturaController : Controller
    {
        private AppDbContext context;
        public CapturaController(AppDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Capturar(int pokemonId)
        {
            var captura = new Captura();
            captura.PokemonId = pokemonId;
            var entrenador= HttpContext.Session.Get<Entrenador>("LoggedUser");
            captura.EntrenadorId = entrenador.Id;
            captura.Fecha = DateTime.Now;
            context.Capturas.Add(captura);
            context.SaveChanges();
            return View ("Capturado");
        }
    }
}